package com.twuc.webApp.Controller;

import com.twuc.webApp.Game;
import com.twuc.webApp.Hint;
import com.twuc.webApp.UserAnswer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class GameController {
    @PostMapping("/api/games")
    @ResponseStatus(value = HttpStatus.CREATED)
    public void createGame() {
    }
    @PostMapping("/api/games/{gameId}")
    public void createGameUri(@PathVariable Integer gameId) {
    }
    @GetMapping("/api/games/{gameId}")
    @ResponseBody
    public ResponseEntity getGameStatusResponse(@PathVariable Integer gameId) {
        if(gameId == null) {
            throw new RuntimeException("gameId not found!");
        }
        return ResponseEntity.ok().body(new Game(2, "1234"));
    }
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity getGameStatusNotFound() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
    @PatchMapping("/api/games/{gameId}")
    @ResponseBody
    public ResponseEntity<Hint> playGameGetHint(@RequestBody UserAnswer userAnswer, @PathVariable Integer gameId) {
        if(gameId == null) {
            throw new RuntimeException("gameId not found!");
        }
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
                .body(new Hint("1A2B", false));
    }

}
