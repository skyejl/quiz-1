package com.twuc.webApp;

public class Hint {
    private String hint;
    private Boolean correct;

    public String getHint() {
        return hint;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public Hint(String hint, Boolean correct) {
        this.hint = hint;
        this.correct = correct;
    }

}
