package com.twuc.webApp.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.Game;
import com.twuc.webApp.Hint;
import com.twuc.webApp.UserAnswer;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class GameControllerTest {
    @Autowired
    private TestRestTemplate testRestTemplate;
    private RestTemplate patchRestTemplate;

    @Test
    void should_create_game_with_no_param() {
        ResponseEntity<Void> entity = testRestTemplate.postForEntity("/api/games", null, Void.class);
        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
    }

    @Test
    void should_create_game_pass_path_variable() {
        ResponseEntity<Void> entity = testRestTemplate.postForEntity("/api/games/2", null, Void.class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
    }

    @Test
    void should_game_status_response() {
        ResponseEntity<Game> entity = testRestTemplate.getForEntity("/api/games/2", Game.class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals("2", entity.getBody().getId().toString());
        assertEquals("1234", entity.getBody().getAnswer().toString());
    }

    @Test
    void should_be_notfound_when_path_variable_not_exist() {
        ResponseEntity<Game> entity = testRestTemplate.getForEntity("/api/games/null", Game.class);
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }


    @Test
    void should_play_game_get_hint() throws JsonProcessingException {
        patchRestTemplate = testRestTemplate.getRestTemplate();
        HttpClient httpClient = HttpClientBuilder.create().build();
        patchRestTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));
        ResponseEntity<Hint> hintResponseEntity = patchRestTemplate
                .postForEntity("/api/games/2", "{ \"answer\": \"1234\" }", Hint.class);
//        HttpHeaders header = new HttpHeaders();
//        header.setContentType(MediaType.APPLICATION_JSON);
//        ResponseEntity hintResponseEntity = patchRestTemplate.exchange("/api/games/2", HttpMethod.PATCH,
//                new HttpEntity<>(new ObjectMapper().writeValueAsString(new UserAnswer("1234")), header), Hint.class);
        assertEquals(HttpStatus.OK, hintResponseEntity.getStatusCode());
    }
}
